//
//  ProductVC.swift
//  eFireplace AR
//
//  Created by CSR63 on 5/3/18.
//  Copyright © 2018 CSR63. All rights reserved.
//

import UIKit

class ProductVC: UIViewController {

    @IBOutlet weak var imageBig: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var placeButton: UIButton!
    
    var searchResult = SearchResult()
    var downloadTask: URLSessionDownloadTask?
    var infoOnly = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailLabel.text = searchResult.description
        nameLabel.text = searchResult.itemName
        placeButton.isHidden = infoOnly
        priceLabel.text = String(format: "$%.2f", searchResult.itemPrice)
        imageBig.image = UIImage(named: "placeholder")
        if let bigURL = URL(string: searchResult.image) {
            downloadTask = imageBig.loadImage(url: bigURL)
        }
    }
    
    deinit {
        downloadTask?.cancel()
        downloadTask = nil
    }

    @IBAction func infoTapped(_ sender: Any) {
        if let link = URL(string: searchResult.productPageURL) {
            UIApplication.shared.open(link)
        }
    }
    
    @IBAction func placeObjectTapped(_ sender: UIButton) {
    }
    @IBAction func favoriteTapped(_ sender: UIButton) {
    }
}
