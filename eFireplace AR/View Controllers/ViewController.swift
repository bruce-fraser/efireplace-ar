//
//  ViewController.swift
//  eFireplace AR
//
//  Created by CSR63 on 4/25/18.
//  Copyright © 2018 CSR63. All rights reserved.
//

import UIKit
import ARKit

class ViewController: UIViewController {

    // MARK: IBOutlets
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var statusLable: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var crosshairImage: UIImageView!
    @IBOutlet weak var trashButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    
    //var nodeNames = [String]()        is this needed???????
    private let search = Search()
    var searchResultToPlace: SearchResult?
    var anyItemsDisplayed: Bool {
        return numberItemsDisplayed < 1 ? false:true
    }
    var numberItemsDisplayed = 0
    var lastDownloadTime: Date?
    var itemArray: [SearchResult]?
    var placingInProcess = false
    var placeTimer: Timer!
    var planeNodesArray = [SCNNode]()
    var selectedNode: SCNNode?
    var itemNodeDictionary = [SCNNode: Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        sceneView.delegate = self
        sceneView.autoenablesDefaultLighting = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.isIdleTimerDisabled = true
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration, options: [.removeExistingAnchors, .resetTracking])
        
        if itemArray == nil || (lastDownloadTime != nil && Date() > lastDownloadTime!.addingTimeInterval(10000)) {
            toggleAddSpinner(spinnerOn: true, addOn: false, message: "Retriving AR inventory items.")
            search.performSearch()
            switch search.state {
            case .notSearchedYet:
                itemArray = nil
                print("not searched yet")
            case .noResults:
                itemArray = nil
                print("no results")
            case .results(let list):
                itemArray = list
                for item in list {
                    print("Item name: \(item.itemName)")
                }
            }
            lastDownloadTime = Date()
        }
        
        if !anyItemsDisplayed && itemArray != nil {
            toggleAddSpinner(spinnerOn: false, addOn: true, message: "Press 'plus' button to select an item.")
        } else if anyItemsDisplayed && itemArray != nil {
            toggleAddSpinner(spinnerOn: false, addOn: true, message: nil)
        }
        if itemArray == nil {
            toggleAddSpinner(spinnerOn: false, addOn: false, message: "No items have been downloaded.  Please reopen app in a few minutes.")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
        placeTimer.invalidate()
    }
    
    func toggleAddSpinner(spinnerOn: Bool, addOn: Bool, message: String?) {
        if let message = message {
            statusLable.text = message
            statusLable.isHidden = false
        } else {
            statusLable.text = ""
            statusLable.isHidden = true
        }
        if spinnerOn {
            spinner.isHidden = false
            spinner.startAnimating()
          
        } else {
            spinner.stopAnimating()
            spinner.isHidden = true
        }
        if addOn {
            addButton.isHidden = false
        } else {
            addButton.isHidden = true
        }
    }
    
    @IBAction func unwindFromProductVC(_ sender: UIStoryboardSegue) {
        if let senderVC = sender.source as? ProductVC {
            searchResultToPlace = senderVC.searchResult
            placeTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(placeItem), userInfo: nil, repeats: true)
            print("and we're back.")
        }
    }
    @objc func placeItem() {
        print(placingInProcess)
        if !placingInProcess {
            toggleAddSpinner(spinnerOn: true, addOn: false, message: "Looking for surface to place item.")
            crosshairImage.isHidden = false
            for i in planeNodesArray {
                i.geometry?.materials.first?.diffuse.contents = UIImage(named: "art.scnassets/grid.png")
            }
            placingInProcess = true
        }
        let hitTest = sceneView.hitTest(view.center, types: .existingPlaneUsingExtent)
        if !hitTest.isEmpty {
            placingInProcess = false
            placeTimer.invalidate()
            toggleAddSpinner(spinnerOn: false, addOn: true, message: nil)
            crosshairImage.isHidden = true
            for i in planeNodesArray {
                i.geometry?.materials.first?.diffuse.contents = UIColor.clear
            }
            let mName = searchResultToPlace!.localModelName!
            let scene = SCNScene(named: "art.scnassets/\(mName)/\(mName).scn")!
            let node = scene.rootNode.childNode(withName: mName, recursively: false)!
            node.position = hitTest.first!.worldTransform.scenePosition
            sceneView.scene.rootNode.addChildNode(node)
            numberItemsDisplayed += 1
            itemNodeDictionary[node] = itemArray?.index(where: { $0 === searchResultToPlace })
            searchResultToPlace = nil
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let touchLocation = touch.location(in: sceneView)
            print(touchLocation.debugDescription)
//            selectedNode?.geometry?.materials.first?.transparent.contents = UIColor.white
            selectedNode?.geometry?.materials.forEach({ (mat) in
                mat.transparent.contents = UIColor.white
            })
            trashButton.isHidden = true
            infoButton.isHidden = true
            selectedNode = nil
            let results = sceneView.hitTest(touchLocation, options: nil)
           
            if !results.isEmpty {
                for n in results {
                    if !(n.node.geometry is SCNPlane) {
//                        n.node.geometry?.materials.first?.transparent.contents = UIColor.white.withAlphaComponent(0.5)
                        n.node.geometry?.materials.forEach({ (mat) in
                            mat.transparent.contents = UIColor.white.withAlphaComponent(0.5)
                        })
                        selectedNode = n.node
                        print(n.node.name!)
                        trashButton.isHidden = false
                        infoButton.isHidden = false
                        break
                    }
                }
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard selectedNode != nil else { return }
        if let touch = touches.first {
            print("touches moved ...")
            let touchLocation = touch.location(in: sceneView)
            let hitTest = sceneView.hitTest(touchLocation, types: .existingPlaneUsingExtent)
            if !hitTest.isEmpty {
                selectedNode?.position = hitTest.first!.worldTransform.scenePosition
            }
        }
    }
    
    @IBAction func didRotate(_ sender: UIRotationGestureRecognizer) {
        guard sender.view != nil, selectedNode != nil else { return }
        if sender.state == .began || sender.state == .changed {
            print(sender.rotation)
            if let sNode = selectedNode {
                sNode.eulerAngles.y -= Float(sender.rotation)
            }
            
            sender.rotation = 0
        }
    }
    
    @IBAction func trashTapped(_ sender: UIButton) {
        if let node = selectedNode {
            itemNodeDictionary[node] = nil
        }
        selectedNode?.removeFromParentNode()
        selectedNode = nil
        numberItemsDisplayed -= 1
        trashButton.isHidden = true
        infoButton.isHidden = true
    }
}

extension ViewController: ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }   // placingInProcess  <-------
        let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
        if placingInProcess {
            plane.materials.first?.diffuse.contents = UIImage(named: "art.scnassets/grid.png")
        } else {
            plane.materials.first?.diffuse.contents = UIColor.clear
        }
        let planeNode = SCNNode(geometry: plane)
        planeNode.position = SCNVector3(CGFloat(planeAnchor.center.x),CGFloat(planeAnchor.center.y), CGFloat(planeAnchor.center.z))
        planeNode.eulerAngles.x = -.pi/2
        planeNodesArray.append(planeNode)
        node.addChildNode(planeNode)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor, let planeNode = node.childNodes.first, let plane = planeNode.geometry as? SCNPlane else { return }
//        if !placingInProcess {
//            planeNode.removeFromParentNode()
//            return
//        }
        plane.width = CGFloat(planeAnchor.extent.x)
        plane.height = CGFloat(planeAnchor.extent.z)
        planeNode.position = SCNVector3(CGFloat(planeAnchor.center.x), CGFloat(planeAnchor.center.y), CGFloat(planeAnchor.center.z))
    }
}

extension ViewController: UIPopoverPresentationControllerDelegate {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if let popoverController = segue.destination.popoverPresentationController, let selectionController = segue.destination.childViewControllers.first as? SelectionTableTableViewController, let button = sender as? UIButton {
            popoverController.delegate = self
            popoverController.sourceView = button
            popoverController.sourceRect = button.bounds
            selectionController.itemArray = itemArray!
        }
        
        if segue.identifier == "InfoSegue" {
            let prodVC = segue.destination as! ProductVC
            print(selectedNode!)
            print(itemNodeDictionary[selectedNode!]!)
            prodVC.searchResult = itemArray![itemNodeDictionary[selectedNode!]!]
            print(prodVC.searchResult)
            prodVC.infoOnly = true
        }
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension float4x4 {
    var scenePosition: SCNVector3 {
        let translation = self.columns.3
        return SCNVector3(translation.x, translation.y, translation.z)
    }
}





//
//  GameViewController.swift
//  SceneDemo
//
//  Created by CSR63 on 5/11/18.
//  Copyright © 2018 Bruce Fraser. All rights reserved.
//

import UIKit
import SceneKit

class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lArray = getPoints(one: Loca(lat: 34.02, lon: -118.2), two: Loca(lat: 25.3, lon: -80.2))
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        let scnView = self.view as! SCNView
        scnView.scene = scene
        scnView.allowsCameraControl = true
        scnView.showsStatistics = true
        let gMono = Monolith()
        scene.rootNode.addChildNode(gMono)
        
        let twoPointsNode1 = SCNNode()
        scene.rootNode.addChildNode(twoPointsNode1.buildLineInTwoPointsWithRotation(
            from: SCNVector3(-0.859, 0.500, -0.14), to: SCNVector3(-0.733, 0.563, -0.394), radius: 0.005, color: .cyan))
        let twoPointsNode2 = SCNNode()
        scene.rootNode.addChildNode(twoPointsNode2.buildLineInTwoPointsWithRotation(
            from: SCNVector3(-0.859, 0.500, -0.14), to: SCNVector3(-0.8912, 0.437, 0.154), radius: 0.005, color: .cyan))
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    struct Loca {
        var lat: Double
        var lon: Double
    }
    
    func getPoints(one: Loca, two: Loca) -> [Loca] {
        var locationsArray = [Loca]()
        var locationsArrayTemp = [Loca]()
        locationsArray.append(one)
        locationsArray.append(two)
        var maxDiff: Double = 20.0
        while maxDiff > 10 {
            let latDiff = abs(locationsArray[1].lat - locationsArray[0].lat)
            let lonDiff = abs(locationsArray[1].lon - locationsArray[0].lon)
            maxDiff = max(latDiff, lonDiff)
            if maxDiff <= 10  { continue }
            for i in 0 ..< locationsArray.count {
                locationsArrayTemp.append(locationsArray[i])
                if i < locationsArray.count - 1 {
                    let newLat = (locationsArray[i + 1].lat + locationsArray[i].lat) / 2
                    let newLon = (locationsArray[i + 1].lon + locationsArray[i].lon) / 2
                    let midLoca = Loca(lat: newLat, lon: newLon)
                    locationsArrayTemp.append(midLoca)
                }
            }
            locationsArray = locationsArrayTemp
            locationsArrayTemp = [Loca]()
        }
        return locationsArray
    }
    
    let lArray = getPoints(one: Loca(lat: 34.02, lon: -118.2), two: Loca(lat: 25.3, lon: -80.2))
    
    for n in lArray {
    print(n)
    }
    
    
}



extension SCNNode {
    
    //    func lineBetweenAirports(fromLat: Double, fromLong: Double, toLat: Double, toLong: Double, rad: CGFloat, color: UIColor) -> SCNNode {
    //
    //    }
    
    func normalizeVector(_ iv: SCNVector3) -> SCNVector3 {
        let length = sqrt(iv.x * iv.x + iv.y * iv.y + iv.z * iv.z)
        if length == 0 {
            return SCNVector3(0.0, 0.0, 0.0)
        }
        return SCNVector3( iv.x / length, iv.y / length, iv.z / length)
    }
    
    func buildLineInTwoPointsWithRotation(from startPoint: SCNVector3,
                                          to endPoint: SCNVector3,
                                          radius: CGFloat,
                                          color: UIColor) -> SCNNode {
        let w = SCNVector3(x: endPoint.x-startPoint.x,
                           y: endPoint.y-startPoint.y,
                           z: endPoint.z-startPoint.z)
        let l = CGFloat(sqrt(w.x * w.x + w.y * w.y + w.z * w.z))
        
        if l == 0.0 {
            // two points together.
            let sphere = SCNSphere(radius: radius)
            sphere.firstMaterial?.diffuse.contents = color
            self.geometry = sphere
            self.position = startPoint
            return self
            
        }
        
        let cyl = SCNCylinder(radius: radius, height: l)
        cyl.firstMaterial?.diffuse.contents = color
        
        self.geometry = cyl
        
        //original vector of cylinder above 0,0,0
        let ov = SCNVector3(0, l/2.0,0)
        //target vector, in new coordination
        let nv = SCNVector3((endPoint.x - startPoint.x)/2.0, (endPoint.y - startPoint.y)/2.0,
                            (endPoint.z-startPoint.z)/2.0)
        
        // axis between two vector
        let av = SCNVector3( (ov.x + nv.x)/2.0, (ov.y+nv.y)/2.0, (ov.z+nv.z)/2.0)
        
        //normalized axis vector
        let av_normalized = normalizeVector(av)
        let q0 = Float(0.0) //cos(angel/2), angle is always 180 or M_PI
        let q1 = Float(av_normalized.x) // x' * sin(angle/2)
        let q2 = Float(av_normalized.y) // y' * sin(angle/2)
        let q3 = Float(av_normalized.z) // z' * sin(angle/2)
        
        let r_m11 = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3
        let r_m12 = 2 * q1 * q2 + 2 * q0 * q3
        let r_m13 = 2 * q1 * q3 - 2 * q0 * q2
        let r_m21 = 2 * q1 * q2 - 2 * q0 * q3
        let r_m22 = q0 * q0 - q1 * q1 + q2 * q2 - q3 * q3
        let r_m23 = 2 * q2 * q3 + 2 * q0 * q1
        let r_m31 = 2 * q1 * q3 + 2 * q0 * q2
        let r_m32 = 2 * q2 * q3 - 2 * q0 * q1
        let r_m33 = q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3
        
        self.transform.m11 = r_m11
        self.transform.m12 = r_m12
        self.transform.m13 = r_m13
        self.transform.m14 = 0.0
        
        self.transform.m21 = r_m21
        self.transform.m22 = r_m22
        self.transform.m23 = r_m23
        self.transform.m24 = 0.0
        
        self.transform.m31 = r_m31
        self.transform.m32 = r_m32
        self.transform.m33 = r_m33
        self.transform.m34 = 0.0
        
        self.transform.m41 = (startPoint.x + endPoint.x) / 2.0
        self.transform.m42 = (startPoint.y + endPoint.y) / 2.0
        self.transform.m43 = (startPoint.z + endPoint.z) / 2.0
        self.transform.m44 = 1.0
        return self
    }
}

