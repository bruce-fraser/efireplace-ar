//
//  SelectionTableTableViewController.swift
//  eFireplace AR
//
//  Created by CSR63 on 5/1/18.
//  Copyright © 2018 CSR63. All rights reserved.
//

import UIKit

class SelectionTableTableViewController: UITableViewController {
    
    var itemArray = [SearchResult]() {
        didSet {
            fireplaceArray = itemArray.filter { $0.itemType == 1 }
            accessoryArray = itemArray.filter { $0.itemType == 2 }
        }
    }
    
    var fireplaceArray = [SearchResult]()
    var accessoryArray = [SearchResult]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return fireplaceArray.count + 1
        } else {
            return accessoryArray.count + 1
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 && indexPath.row == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "FirstHeadingCell", for: indexPath)
        } else if indexPath.section == 1 && indexPath.row == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "Heading2Cell", for: indexPath)
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell
            if indexPath.section == 0 {
                cell.configure(for: fireplaceArray[indexPath.row - 1])
            } else {
                cell.configure(for: accessoryArray[indexPath.row - 1])
            }
            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 44
        } else {
            return 88
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        navigationController?.navigationBar.tintColor = UIColor.orange
//    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
//   MARK: - Navigation

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         let productVC = segue.destination as! ProductVC
         productVC.infoOnly = false
         let indexPath = tableView.indexPathForSelectedRow!
        if indexPath.section == 0 {
            productVC.searchResult = fireplaceArray[indexPath.row - 1]
        } else {
            productVC.searchResult = accessoryArray[indexPath.row - 1]
        }
    }
 

}
