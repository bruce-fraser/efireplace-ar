//
//  SearchResult.swift
//  eFireplace AR
//
//  Created by CSR63 on 4/26/18.
//  Copyright © 2018 CSR63. All rights reserved.
//

import Foundation

class ResultArray: Codable {
    var resultCount = 0
    var results = [SearchResult]()
}

class SearchResult: Codable {
    
    var itemType = 0
    var itemName = ""
    var itemPrice: Double = 0.0
    var image = ""
    var modelURL = ""
    var description = ""
    var productPageURL = ""
    
    var localModelName: String?
    
    var category: String {
        switch self.itemType {
        case 1: return "Fireplace"
        case 2: return "Accessory"
        default: return "error"
        }
    }
}
