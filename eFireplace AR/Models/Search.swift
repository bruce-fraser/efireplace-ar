//
//  Search.swift
//  eFireplace AR
//
//  Created by CSR63 on 4/26/18.
//  Copyright © 2018 CSR63. All rights reserved.
//

import Foundation

let f1 = "{ \"itemType\": 1, \"itemName\": \"Cool Fireplace 1\", \"itemPrice\": 34.95, \"image\": \"https://placeimg.com/250/250/tech\", \"modelURL\": \"http://www.yada.com\", \"localModelName\": \"Chair2\", \"description\": \"Details for Cool Fireplace 1.  More deatails like color and stuff.\", \"productPageURL\": \"https://www.efireplacestore.com/sup-wrt3042rs.html\" },"
let f2 = "{ \"itemType\": 1, \"itemName\": \"Cool Fireplace 2\", \"itemPrice\": 434.95, \"image\": \"https://placeimg.com/250/250/animals\", \"modelURL\": \"http://www.yada.com\", \"localModelName\": \"Ship\", \"description\": \"Details for Cool Fireplace 2.  More deatails like color and stuff.\", \"productPageURL\": \"https://www.efireplacestore.com/sup-wrt3042rs.html\" },"
let f3 = "{ \"itemType\": 2, \"itemName\": \"Cool Accessory 3\", \"itemPrice\": 834.95, \"image\": \"https://placeimg.com/250/250/animals\", \"modelURL\": \"http://www.yada.com\", \"localModelName\": \"Chair2\", \"description\": \"Details for Cool Accessory 3.  More deatails like color and stuff.\", \"productPageURL\": \"https://www.efireplacestore.com/sup-wrt3042rs.html\" },"
let f4 = "{ \"itemType\": 1, \"itemName\": \"Cool Fireplace 4\", \"itemPrice\": 334.95, \"image\": \"https://placeimg.com/250/250/tech\", \"modelURL\": \"http://www.yada.com\", \"localModelName\": \"Chair2\", \"description\": \"Details for Cool Fireplace 4.  More deatails like color and stuff.\", \"productPageURL\": \"https://www.efireplacestore.com/sup-wrt3042rs.html\" },"
let f5 = "{ \"itemType\": 1, \"itemName\": \"Cool Fireplace 5\", \"itemPrice\": 934.95, \"image\": \"https://placeimg.com/250/250/animals\", \"modelURL\": \"http://www.yada.com\", \"localModelName\": \"earth\", \"description\": \"Details for Cool Fireplace 5.  More deatails like color and stuff.\", \"productPageURL\": \"https://www.efireplacestore.com/sup-wrt3042rs.html\" },"
let f6 = "{ \"itemType\": 2, \"itemName\": \"Cool Accessory 6\", \"itemPrice\": 934.95, \"image\": \"https://placeimg.com/250/250/tech\", \"modelURL\": \"http://www.yada.com\", \"localModelName\": \"earth\", \"description\": \"Details for Cool Accessory 6.  More deatails like color and stuff.\", \"productPageURL\": \"https://www.efireplacestore.com/sup-wrt3042rs.html\" },"
let f7 = "{ \"itemType\": 2, \"itemName\": \"Cool Accessory 7\", \"itemPrice\": 934.95, \"image\": \"https://placeimg.com/250/250/animals\", \"modelURL\": \"http://www.yada.com\", \"localModelName\": \"earth\", \"description\": \"Details for Cool Accessory 7.  More deatails like color and stuff.\", \"productPageURL\": \"https://www.efireplacestore.com/sup-wrt3042rs.html\" },"
let f8 = "{ \"itemType\": 1, \"itemName\": \"Cool Fireplace 8\", \"itemPrice\": 964.95, \"image\": \"https://placeimg.com/250/250/tech\", \"modelURL\": \"http://www.yada.com\", \"localModelName\": \"Ship\", \"description\": \"Details for Cool Fireplace 8.  More deatails like color and stuff.\", \"productPageURL\": \"https://www.efireplacestore.com/sup-wrt3042rs.html\" },"
let f9 = "{ \"itemType\": 1, \"itemName\": \"Superior WRT3042 Pro Series 42\u{201d} Radiant Wood-Burning Fireplace\", \"itemPrice\": 964.95, \"image\": \"https://placeimg.com/250/250/animals\", \"modelURL\": \"http://www.yada.com\", \"localModelName\": \"couch1\", \"description\": \"Details for Cool Fireplace 9.  More deatails like color and stuff.\", \"productPageURL\": \"https://www.efireplacestore.com/sup-wrt3042rs.html\" }"

//let objectsString = "{ \"resultCount\": 8, \"results\": [" + f1 + f2 + f3 + f4 + f5 + f6 + f7 + f8 + f9 + "]}" //f6 + f7 + f8 +
//let data: Data? = objectsString.data(using: .utf8)

class Search {
    enum State {
        case notSearchedYet
        case noResults
        case results([SearchResult])
    }
    
    private(set) var state: State = .notSearchedYet
    
    func performSearch() {
        var newState = State.notSearchedYet
        let path = Bundle.main.path(forResource: "inputFile", ofType: "json")!
        let data = try? Data(contentsOf: URL(fileURLWithPath: path))
        let searchResults = parse(data: data!)
        if searchResults.isEmpty {
            newState = .noResults
        } else {
            newState = .results(searchResults)
        }
        state = newState
    
    }
    
    private func parse(data: Data) -> [SearchResult] {
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(ResultArray.self, from: data)
            return result.results
        } catch {
            print("JSON Error: \(error)")
            return []
        }
    }
    
}
